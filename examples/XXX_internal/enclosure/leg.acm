/* A model of a leg.
 * By Yingfu Zeng, Jawad Masood and Adam Duracz
 * Implemented as a constrained double pendulum.
 */
class DoublePendulum (theta1, theta2)
  private
    theta1' := 0; theta1'' := 0;
    theta2' := 0; theta2'' := 0;
    l1 := 1; l2 := 1;
    m1 := 1; m2 := 1; 
    g := 9.8;
    mode := "";
    /* For visualization */
    offset   := 1;
    ball1P := [1*sin(theta1),0,-1*cos(theta1)+1];
    ball2P := [1*sin(theta1)+1*sin(theta2),0,-1*cos(theta1)-1*cos(theta2)+1];
    line1P := [0.5*sin(theta1),0,-0.5*cos(theta1)+1];
    line2P := [1*sin(theta1)+0.5*sin(theta2),0,-1*cos(theta1)-0.5*cos(theta2)+1];
    _3D := [["Sphere",  [0,0,0],0.1,[0.8,0.1,0.1],[0,0,0]],
            ["Cylinder", [0,0,0],[0.02,1],[0.1,0.1,0.8],[0,0,0]],
            ["Sphere",   [0,0,0],0.1,[0.8,0.1,0.1],[0,0,0]],
            ["Cylinder", [0,0,0],[0.02,1],[0.1,0.1,0.8],[0,0,0]]];
  end
 
  switch mode
    case ""
      if theta1 >= 0 && theta1' > 0
        theta1 := -theta1;
        theta1' := -0.9*theta1';
        mode := "";
      end;
      if (theta2 <= 0 && theta2' < 0) || (theta2 >= pi/4 && theta2' > 0)
        theta2 := -theta2;
        theta2' := -0.9*theta2';
        mode := "" 
      end;
      /* Derived from Euler-Lagrange equations */
      theta1'' = (-m2*l2*theta2''*cos(theta1 - theta2) + m2*l2*theta2'^2*sin(theta1 - theta2) - g*(m1 + m2)*sin(theta1))/((m1+m2)*l1);
      theta2'' = (-m2*l1*theta1''*cos(theta1 - theta2) - m2*l1*theta1'^2*sin(theta1 - theta2) - m2*g*sin(theta2)) / (m2*l2);
  end;
  /* For visualization */
  ball1P := [l1*sin(theta1),0,-l1*cos(theta1)+offset];
  ball2P := [l1*sin(theta1)+l2*sin(theta2),0,-l1*cos(theta1)-l2*cos(theta2)+offset];
  line1P := [0.5*l1*sin(theta1),0,-0.5*l1*cos(theta1)+offset];
  line2P := [l1*sin(theta1)+0.5*l2*sin(theta2),0,-l1*cos(theta1)-0.5*l2*cos(theta2)+offset];
  _3D := [["Sphere",  ball1P,  0.1,       [0.8,0.1,0.1], [0,     0,     0]],
          ["Cylinder", line1P,  [0.01,l1], [0.1,0.1,0.8], [3.14/2,theta1,0]],
          ["Sphere",   ball2P,  0.05,      [0.8,0.1,0.1], [0,     0,     0]],
          ["Cylinder", line2P,  [0.01,l2], [0.1,0.1,0.8], [3.14/2,theta2,0]]];
end

class Main(simulator)
  private
    mode := "Initialize";
  end
  switch mode
    case "Initialize"
      simulator.endTime := 20;
      create DoublePendulum(-1.5,0.3);
      mode := "Persist";
    case "Persist"
  end
end