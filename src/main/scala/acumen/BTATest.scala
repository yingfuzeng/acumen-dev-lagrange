package acumen
object BTATest{

  val test1 = 
  """
  class main(simulator)
  private r := 0; y := 0; y' := 1 end
    y' = 1;
    r = let 
	  x = 2
	in y end;
  end
  """

  val test2 = 
  """
  class main(simulator)
  private y := 0; y' := 0 end
   y' = 1;
   y = 10;
  end
  """

  val test3 = 
  """
  class Main(simulator)
  private x := 0; y := 0;end
   y = x + 1;
   if(x < 0)
     x = 1;
   else
     x = 2
   end;
   
  end
  """

  val test4 = 
  """
  class Main(simulator)
  private q := [0];m:=0;g:=0;l:=0;T:=0;I:=0;V:=0;L:=0;
          theta := pi/4; theta' := 0; theta'' := 0;
  end
    q = [theta];   
    m = 5;
    g = 9.8;
    l = 3;
    I = m * l ^2;
    T = 0.5 * I * theta'^2;
    V = m * g * l * (1 - cos(theta));
    L = T - V;
    (L'[(q[0])'])' - L'[q[0]] = 0;
  end
  """


  val test41 = 
  """
  class Main(simulator)
  private q := [0];m:=0;g:=0;l:=0;T:=0;I:=0;V:=0;L:=0;
          theta := pi/4; theta' := 0; theta'' := 0;
  end
    L = T - V;
    L'[I] = 0;
  end
  """

 val test5 = 
 """
  class Main(simulator)
  private q := [0,0];a := 1; m := 2; M := 5; g := 0; k := 2; T:=0;I:=0;V:=0;L:=0;
          t := pi/4; t' := 0; t'' := 0;
          x := 1; x' := 0; x'' := 0;
  end

    q = [x, t];
    k = 2;
    g = -9.8;
    a = 1;
    m = 2;
    M = 5;
    T = 0.5 * (M + m) * x'^2 + m*a*x'*t'*cos(t) + (2/3)*m * a^2*t'^2;
    V = 0.5*k*x^2 + m*g*a*(1-cos(t));
    L = T - V;
 //  L  = x'*t'*cos(t);  
  foreach i in length(q) begin
      L'[q[i]']' - L'[q[i]] = 0;
    end;

  end
  """
 val test6 = 
 """
  class Main(simulator)
  private q := [0,0];a := 1; m := 2; M := 5; g := 0; k := 2; T:=0;I:=0;V:=0;L:=0;
          t := 0; t' := 0; t'' := 0;
          x := 0; x' := 0; x'' := 0;
  end
  (98/5)*sin(t) + 2*cos(t)*x'' + (8/3)*t'' = 0;
  2*cos(t)*t'' - 2*sin(t)*t'*t' + 7*x'' +2*x = 0;


  end
  """
  
 // Double pendulum
 val test7 = 
 """
  class Main(simulator)
  private q := [0,0];m1 := 1; m2:= 1; g := 0;l1 := 0;l2:=0; T:=0;V:=0;L:=0;
          t1 := pi/4; t1' := 0; t1'' := 0;
          t2 := pi/4; t2' := 0; t2'' := 0;
  end
    q = [t1,t2];
    g = 9.8;
    l1 = 1;
    l2 = 1;
    m1 = 1;
    m2 = 1;
    T = 0.5*m1*l1^2*t1'^2 + 0.5*m2*(l1^2*t1'^2 + l2^2*t2'^2 + 2*l1*l2*t1'*t2'*cos(t1-t2));
    V = -1*(m1 + m2)*g*l1*cos(t1) - m2*g*l2*cos(t2);
    L = T - V;
  foreach i in length(q) begin
      L'[q[i]']' - L'[q[i]] = 0;
    end;
  end
  """
 val test8 = 
  """
  class A(p,q)
    p = 1;
    q = 2;
  end
  class Main(simulator)
  private
    a := create A(0,0);
  end
    a.p = 2;
    a.q = 2;
  end
  """  
 val test9 =
  """
  class Main(simulator)
  private
   t := 1;
   y := 0;
  end
  t = 1;
  switch t
    case 0
      y = 1;
    case 1
      y = 3;
  end;
  end
  """  
val test10 = 
  """
 class Main(simulator)
  private
   t := 1;
   y := 0;
   r :=0;
  end
  r = y + 2;
  switch t
    case 0
      y = 1;
    case 1
      y = 3;
  end;
  end 
  """


}
