package acumen

object TestSuiteForDAE {
  import util.Names.name
  def int(x:Int) = new Lit(GInt(x)) 
  def times(x:Expr,v:Expr) = new Op(name("*"),x::v::Nil)
  def plus(xs:Expr*) = new Op(name("+"),xs.toList)
  def minus(x:Expr,v:Expr) = new Op(name("-"),x::v::Nil)
  def sin(x:Expr) =     new Op(name("sin"),x::Nil)
 
  val x = new Var(name("x"));
  val t = new Var(name("t"));
  val tp = new Var(Name("t",1));
  val t2p = new Var(Name("t",2));
  val xp = new Var(Name("x",1));
   val x1 = new Var(name("x1"));
   val x2 = new Var(name("x2"));
   val x3 = new Var(name("x3"));
   val x4 = new Var(name("x4"));
   val y = new Var(name("y"));
   val z = new Var(name("z"));
   val w = new Var(name("w"));
   val e1 = new Equation(Op(name("+"), x::Nil),Lit(GInt(1)))
   val e2 = new Equation(Op(name("+"), y::Nil),Lit(GInt(2)))
   val e3 = new Equation(Op(name("+"),x::y::z::Nil),Lit(GInt(4)))
   val e4 = new Equation(Op(name("+"),x::y::z::w::Nil),Lit(GInt(4)))

   val test1 = e1::e2::e3::e4::Nil
   val test1q = x::y::z::w::Nil
  

   // Tarjan example in the book chapter 7
   val u0 = new Var(name("u0"));
   val i0 = new Var(name("i0"));
   val u1 = new Var(name("u1"));
   val i1 = new Var(name("i1"));
   val u2 = new Var(name("u2"));
   val i2 = new Var(name("i2"));
   val uL = new Var(name("uL"));
   val diL = new Var(name("diL"));
   val iC = new Var(name("iC"));
   val duC = new Var(name("duC"));
  
  // Equations, assume all Constant to be 1
  // If we diL is a variable, we view iL as a known variable presented by 1.
   val e21 = new Equation(u0,Lit(GInt(1)))
   val e22 = new Equation(u1,i1)
   val e23 = new Equation(u2,i2)
   val e24 = new Equation(uL,diL)
   val e25 = new Equation(iC,duC)
   val e26 = new Equation(u0,Op(name("+"), u1::Lit(GInt(1))::Nil))
   val e27 = new Equation(uL,Op(name("+"), u1::u2::Nil))
   val e28 = new Equation(Lit(GInt(1)),u2)
   val e29 = new Equation(i0,Op(name("+"), i1::Lit(GInt(1))::Nil))
   val e210 = new Equation(i1,Op(name("+"), i2::iC::Nil))
   
   val test = e210::e29::e28::e27::e26::e25::e24::e23::e22::e21::Nil
   val test2 = test.reverse
   
   // x + y = 2; x - y = 1;
   val e31 = new Equation(Op(name("+"), x::y::Nil),Lit(GInt(2)))
   val e32 = new Equation(Op(name("-"), x::y::Nil),Lit(GInt(1)))
   val test3 = e32::e31::Nil
   
   // Algebric loop examples
   val di1 = new Var(name("di1"))
   val di2 = new Var(name("di2"))
  
   val e41 = new Equation(Op(name("-"),
                             Op(name("-"),Lit(GInt(1))::u1::Nil)::u2::Nil),Lit(GInt(0)))
   val e42 = new Equation(Op(name("-"), u1::di1::Nil), Lit(GInt(0)))
   val e43 = new Equation(Op(name("-"), u2::di2::Nil), Lit(GInt(0)))
   val e44 = new Equation(Op(name("-"), di1::di2::Nil), Lit(GInt(0)))
   val test4 = e44::e43::e42::e41::Nil

   // Gaussian elimination examples
   val e51 = new Equation(Op(name("+"), Op(name("*"),int(2)::x2::Nil)::x3::Nil), int(-8))
   val e52 = new Equation(plus(x1,times(int(-2),x2),times(int(-3),x3)),int(0))
   val e53 = new Equation(plus(times(int(-1),x1),x2,times(int(2),x3)),int(3))
   val test5 = e52::e51::e53::Nil
   val test5q = x1::x2::x3::Nil

   val e61 = new Equation(plus(times(int(2),x1),plus(minus(x2,x3),times(int(2),x4))),int(5))
   val e62 = new
   Equation(plus(times(int(4),x1),plus(minus(times(int(5),x2),times(int(3),x3)),times(int(6),x4))),int(9))
   val e63 = new
   Equation(plus(times(int(-2),x1),plus(minus(times(int(5),x2),times(int(3),x3)),times(int(6),x4))),int(4))
   val e64 = new
   Equation(plus(times(int(4),x1),plus(minus(times(int(11),x2),times(int(4),x3)),times(int(8),x4))),int(2))
   val test6 = e64::e63::e62::e61::Nil
   val test6q = x1::x2::x3::x4::Nil   


   val e71 = new Equation(plus(plus(x1,x2),x3),int(3))
   val e72 = new Equation(plus(minus(x1,x2),x3),int(1))
   val e73 = new Equation(minus(plus(x1,x2),x3),int(1))
   val test7 = e73::e72::e71::Nil
   
   
   val e81 = new Equation(plus(plus(x1,x2),x3),int(3))
   val e82 = new Equation(plus(plus(minus(x1,x2),x3),x4),int(4))
   val e83 = new Equation(plus(minus(plus(x1,x2),x3),x4),int(0))
   val e84 = new Equation(plus(minus(plus(x1,x2),x3),x4),int(2))
   val test8 = e84::e83::e82::e81::Nil


  val test9 = times(int(-1), minus(times(int(2),x), times(int(3),sin(x)))) 

  //minus(Op(name("sin"), x ::Nil), plus(int(2),plus(int(2),times(int(2),times(int(3),xp))))) 
     //plus(times(int(2),x),times(Op(name("cos"),x::Nil),xp))
     //times(int(-1), times(int(2),x))
    // minus(int(2), times(int(2),x))
}
