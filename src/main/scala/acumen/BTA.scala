/**
 * Binding time analysis
 * Authors: Yingfu Zeng, Walid Taha
 */

package acumen
import Pretty._
import BTATest._
import Simplifier._
import GE._
object BindingTimeAnalysis {
  import Specialization._
  type Label = Int
  def run(t: Prog) = t match {
    case Prog(defs) =>
      Prog(defs.map(d => d match {
        case ClassDef(n,f,d,b) =>
	  bta(ClassDef(n,f,d,b))
      }))
  }
  
  def bta(ast:ClassDef):ClassDef = {
    var anActions = List[AAction[Label]]()
    val aprog = labelAst(ast,0)
    /* Intermedia result log file */
    val filename = "Btalog.txt"
    var header = "Labeled AST \n \n"
    writeToFile(filename, header)
    writeToFile(filename, aprog._1.toString + "\n")
    println(aprog._1)
    anActions = aprog._1.body
    val env = aprog._2
    val constraints = anActions.foldLeft(List[Constraint]())((r,x)=> r ::: traversal(x,env))
    println("Constraints")
    constraints.map(println(_))
    
    header = "Generated constriants  \n"
    writeToFile(filename, header)
    writeToFile(filename, constraints.mkString(";"))

    println("CCS computation result")
    val ccs = CCS(constraints)
    ccs.map(println(_))    
    
    header = "Resolved constriants results  \n"
    writeToFile(filename, header)
    writeToFile(filename, ccs.mkString(";"))

    val btaResult = ccs.map(x => (x.label, x)).toMap 

    // topological sort for known equations only, unknowns are put at tail
    val knowns = (anActions.filter(e => btaResult(e.an).isInstanceOf[Known]))
    val equations = knowns.filter(x => x.isInstanceOf[AContinuously[Label]]).map(y => y match{
		    case AContinuously(c,l) => c}).asInstanceOf[List[AEquation[Label]]]
    val order = Specialization.topoSort(equations)
    
    println("Topological sorting result")
   // order.map(println(_))
    // Sort the equations 

    var orderedEquations = List[AEquation[Label]]()
    for(l <- order.reverse){
      orderedEquations = equations.filter(x => x.an == l) :::
			 orderedEquations
    }
    val unknownEquations = anActions.diff(orderedEquations.map(x => AContinuously(x,x.an)))
    val oactions = orderedEquations.map(x => AContinuously(x,x.an)) ::: unknownEquations
   // println("Orderd equations")
   // orderedEquations.map(println(_))
    //END TEST 

    println("Specialization pharse")
    var senv = Map[Expr,Expr]()
    var result = List[Action]()
    for(a <- oactions){
      val temp = specialize(a,btaResult,senv)
      result = temp._1 ::: result
      senv = temp._2
    }
    result.map(println(_))
    // Replace the body of class
    ast match{
      case ClassDef(n,f,p,body) =>
	ClassDef(n,f,p,gaussianElimination(result.reverse))
    }
   
  }
  
  // Pick the equations from input actions, for what GE will be used to transfering to explicit ODE form
  def gaussianElimination(actions:List[Action]):List[Action] = {
    // Filter all the equations first
    val equations = actions.filter(x => x.isInstanceOf[Continuously])
    val remainActions = actions.diff(equations)
    // Pick all the implict equations 
    var implicitOdes = List[Equation]()
    var explicitOdes = List[Action]()
    for(e <- equations){
      e match{
	case Continuously(Equation(lhs,rhs)) => lhs match{
	  case Var(_) => explicitOdes = e :: explicitOdes
	  case Dot(_,_) => explicitOdes = e :: explicitOdes
	  case _ => implicitOdes = Equation(lhs,rhs) :: implicitOdes
	}
	  
	case _ =>
      }
    }

   // Finding all the variables to be solved in the implicit ODEs

   var vars:List[Var] = implicitOdes.map(x => findVars(x.lhs) ::: findVars(x.rhs)).flatten.toSet.toList
   vars.map(println(_))
   // Only the highest order variables are variables to be solved, get rid of the lower order ones
   var trueVars = vars.filter(x => !(vars.exists(y => (y.name.x == x.name.x && y.name.primes > x.name.primes))))
    
   println("True vars:")
   trueVars.map(println(_))
   println("Implicit equations to be solved :")
   implicitOdes.map(x => println(pprint(x.lhs) + " = " + pprint(x.rhs)))
   println("Try to simplify implicit equations")
   val simplicitOdes = implicitOdes.map(x => simplifyEquation(x))

   var odes = GE.run(simplicitOdes, trueVars).map(x => Continuously(simplifyEquation(x)))
   println("ODEs are :")
   odes.map(x => x.a match{
     case Equation(lhs,rhs) => println(pprint(lhs) + " = " + pprint(rhs))
   })
   remainActions ::: explicitOdes ::: odes
  }
  /* Annotate expression with a label, return the annotated ast and the next available label */
  def labelAst(e:Expr, label:Label):(AExpr[Label], Label) = e match {
    case Lit(gv) => (ALit(gv, label), label+1) 
    case Var(name) => (AVar(name, label), label+1) 
				      
    case Op(f,es) =>
      var aes:List[AExpr[Label]] = List.empty
      var nextLabel = label
      for(ee <- es){
	val result = labelAst(ee, nextLabel)
	aes = result._1 :: aes
	nextLabel = result._2
      }
      (AOp(f,aes.reverse,nextLabel), nextLabel + 1)
    case ExprVector(ls) => {
      var nextLabel = label
      var aExprVector = List[AExpr[Label]]()
      for(l <- ls){
        var aexpr = labelAst(l, nextLabel)
	aExprVector = aexpr._1 :: aExprVector
	nextLabel = aexpr._2
      }
      (AExprVector(aExprVector.reverse, nextLabel), nextLabel+1)
    }
    case Dot(expr, name ) => {
      val aexpr = labelAst(expr, label)
      (ADot(aexpr._1, name, aexpr._2), aexpr._2 + 1)
    }
    case ExprLet(bindings,expr) => {
      var nextLabel = label
      var abindings = List[(AVar[Label],AExpr[Label])]()
      for(binding <- bindings){
        val avar = AVar(binding._1, nextLabel)
	val arhs = labelAst(binding._2, nextLabel+1)
	abindings = (avar, arhs._1) :: abindings
	nextLabel = arhs._2
	
      }
      val aexpr = labelAst(expr, nextLabel)
      (AExprLet(abindings.reverse, aexpr._1, aexpr._2), aexpr._2 + 1)
    } 
  }
  def labelAst(a:Action, label:Label):(AAction[Label], Label) = a match {
    case Continuously(a) => a match {
      case Equation(lhs, rhs) => 
	val alhs = labelAst(lhs,label)
        val arhs = labelAst(rhs,alhs._2)
	// Really redundent here, repeat label twice
	(AContinuously(AEquation(alhs._1, arhs._1, arhs._2), arhs._2),arhs._2 + 1)
    }
    case Discretely(a) => a match {
      case Assign(lhs, rhs) => 
	val alhs = labelAst(lhs,label)
        val arhs = labelAst(rhs,alhs._2)
	// Really redundent here, repeat label twice
	(ADiscretely(AAssign(alhs._1, arhs._1, arhs._2), arhs._2),arhs._2 + 1)
      case Create(x,name,args) => (ADiscretely(ACreate(x,name,args,label), label), label + 1)
      case Elim(e) => 
        val aexpr = labelAst(e,label)
        (ADiscretely(AElim(aexpr._1, aexpr._2), aexpr._2), aexpr._2 + 1)
      case Move(obj,newParent) =>
        val aobj = labelAst(obj,label)
        val ap = labelAst(newParent, aobj._2)
        (ADiscretely(AMove(aobj._1, ap._1,ap._2), ap._2), ap._2 + 1)
    }

      

    case IfThenElse(cond,t,e) =>
      val acond = labelAst(cond,label)
      val at = labelListAst(t, acond._2)
      val ae = labelListAst(e, at._2)
      (AIfThenElse(acond._1, at._1, ae._1, ae._2), ae._2 + 1)

    case Switch(cond,clauses) => 
      val acond = labelAst(cond,label)
      val aClauses = labelListClauses(clauses, acond._2)
      (ASwitch(acond._1, aClauses._1, aClauses._2), aClauses._2 + 1)
    case EquationFamily(i,q,es) =>
      val aes =  labelListAst(es.map(x=>Continuously(x)),label+2)
      val equations:List[AEquation[Label]] = aes._1.map(x => x match{
	case AContinuously(c,l) => c
	case _ => error("Can't have non-equations in the equation family construct")
      }).asInstanceOf[List[AEquation[Label]]]
      (AEquationFamily(AVar(i.name,label), AVar(q.name, label+1),
		       equations, aes._2),
       aes._2 + 1)
  }

 def labelAst(ast:Prog, label:Label):(AProg[Label],EnvVars) = {
   var env = new EnvVars()
   var counter = label
   ast match{
      case Prog(defs) =>
	(AProg(defs.map(d => d match {
          case ClassDef(c, f, p, b) =>
	    {
	      val aClassDef = labelAst(ClassDef(c, f, p, b),counter)
              env = env ++ aClassDef._2
	      counter = aClassDef._3
	      aClassDef._1
	    }})), env)
    }
 }
 def labelAst(ast:ClassDef, label:Label):(AClassDef[Label],EnvVars,Label) = {
   var env = new EnvVars()
   var counter = label
   ast match{
          case ClassDef(cName, fields, privs, body) =>
	    ({
	      val initResult = labelListAst(privs, label, env)
	      env = initResult._3
              var afields = List[AName[Label]]()
	      var counter = initResult._2
	      for(field<-fields){
		afields = AName(field, counter) :: afields
		env += Var(field) -> counter
		counter = counter + 1
	      }
	      val actionResult = labelListAst(body, counter)
	      counter = actionResult._2
	      AClassDef(cName, afields, initResult._1, actionResult._1)
	      
	    }, env,counter)
    }
 }

 def labelListAst(actions:List[Action], initLabel:Label):(List[AAction[Label]],Label) = {
   var nextLabel = initLabel
   var result = List[AAction[Label]]()
   for(action <- actions){
     val temp  = labelAst(action, nextLabel)
     result = temp._1 :: result
     nextLabel = temp._2
   }
   (result.reverse, nextLabel)
 }
 // Label a list of Inits, return the next available label and
 // the environment with variable -> label 
 def labelListAst(inits:List[Init], initLabel:Label, initEnv:EnvVars):(List[AInit[Label]],Label,EnvVars)={
   var nextLabel = initLabel
   var ainits = List[AInit[Label]]()
   var env = new EnvVars()
   for(init <- inits){
     ainits = AInit(AVar(init.x, nextLabel), init.rhs) :: ainits
     env += Var(init.x) -> nextLabel
     nextLabel = nextLabel + 1
   }
   (ainits.reverse, nextLabel, env)
 }
 
 def labelListClauses(clauses:List[Clause], initLabel:Label):(List[AClause[Label]],Label)={
   var nextLabel = initLabel
   var aClauses = List[AClause[Label]]()
   for(clause <- clauses){
     val aasertion = labelAst(clause.assertion, nextLabel)
     val arhs = labelListAst(clause.rhs,aasertion._2)
     val aclause = AClause(clause.lhs, aasertion._1,arhs._1, arhs._2)
     aClauses = aclause :: aClauses
     nextLabel = arhs._2 + 1
   }
   (aClauses, nextLabel)
 }
 
  // Map with bindings of variable to its label, which stay constant during BTA pharse
  type EnvVars = scala.collection.mutable.HashMap[Var, Label]
  var envVars = new EnvVars()
  type LabelKnowledge = scala.collection.mutable.HashMap[Int, Boolean]

  /* Global counter for making lable */
  var labelCounter:Int = 0
  val emptyCs:List[Constraint] = List.empty

  
  // Traversal ast and generate constraints
  def traversal(e:AExpr[Label], env:EnvVars):List[Constraint] = e match{
    case ALit(gv,l) => List(Known(l))
    case AVar(name,l) => 
      if(name.x != "pi") 
        List(NLT(env(Var(name)), l))
      else
        Nil
    case AOp(f,es,l) => f.x match{
//      case "dif" =>
	// Symbolic differentiation should always be done in compile time
//	Known(l) :: es.map(x => NLT(x.an, l))
      case _ => 
	// <F(e_i, env)> && 
	es.foldLeft(List[Constraint]())((r,x) => traversal(x,env) ::: r) :::
	// <NLT(l_i, l)>
	es.map(x => NLT(x.an, l))
    }
    case ADot(aexpr, n,l) => List(Known(l))
    case AExprVector(es, l) => 
      Known(l) ::
      es.foldLeft(List[Constraint]())((r,x) => traversal(x,env) ::: r)
   
    case AExprLet(bindings,expr,l) => {
      // <F(e_i, env)> &&
      bindings.foldLeft(List[Constraint]())((r,x) => traversal(x._2, env) ::: r) :::
      // <NLT(el_i, l_i)> &&
      bindings.map(x => NLT(x._2.an, x._1.an)) :::
      // NLT(le,l) &&
      List(NLT(expr.an, l)) :::
      // F(e@le, env@@ <x_i -> l_i>)
      traversal(expr,bindings.foldLeft(env)((r,x) => r += (Var(x._1.name) -> x._1.an)))
      
    }
  }
 
  // Return annotated action and the next available label
  def traversal(a:AAction[Label], env:EnvVars):List[Constraint] = a match{ 
    case AContinuously(c,_) => c match {
      case AEquation(lhs,rhs,l) => lhs match{
	case AVar(name, _) => name.primes match{
	  // F(x = e@le @l, Env)
	  case 0 => NLT(l, env(Var(name))) :: 
		    NLT(rhs.an, l) ::
		    traversal(rhs,env)
	  // F(x' = e@le @l, Env)
	  case n => NLT(l, env(Var(name))) ::
	            NLT(rhs.an, l) ::
		    (for(i <- 0 to n-1) yield
		       Unknown(env(Var(Name(name.x, i))))
		    ).toList :::
		    traversal(rhs,env)
		   }
	case _ => NLT(lhs.an,l) :: NLT(rhs.an,l) ::
		  (traversal(lhs,env) :::
	          traversal(rhs,env))
      }
    }
    case ADiscretely(c,l1) => c match {
      case AAssign(lhs,rhs,l) => lhs match{
	// Discrete assignment will be regarded as unknown and leave as it is 
	case AVar(name, _) => Unknown(l) :: Unknown(l1) :: Nil
        case ADot(_,_,_) => Unknown(l) :: Unknown(l1) :: Nil
	case _ => error("The lhs of discrete assignment " + c.toString +" can only be a variable")  
      }
      case ACreate(_,_,_,l) => Unknown(l) :: Nil
      case AElim(_,l) => Unknown(l) :: Nil
      case AMove(_,_,l) => Unknown(l) :: Nil
    }
		   
    case AIfThenElse(cond,t,e,l) => 
      NLT(cond.an,l) ::
      t.map(x=>NLT(x.an,l)) :::
      e.map(x=>NLT(x.an,l)) :::
      t.map(x=>NLT(cond.an,x.an)) :::
      e.map(x=>NLT(cond.an,x.an)) :::
      traversal(cond,env) :::
      t.foldLeft(List[Constraint]())((r,x) => traversal(x,env) ::: r) :::
      e.foldLeft(List[Constraint]())((r,x) => traversal(x,env) ::: r) 

    case AEquationFamily(i,q,aes,l) => 
      val aas = aes.map(x => AContinuously(x,l))
      val newEnv:EnvVars = env += (Var(i.name) -> i.an)
      NLT(env(q.expr),i.an) ::
      NLT(env(q.expr),l) ::
      aes.map(x => NLT(x.an, l)) :::
      aas.foldLeft(List[Constraint]())((r,x) => traversal(x,newEnv) ::: r) 
      
  // case ADiscretely(d) =>  ADiscretely(traversal(d), traversal(d).an)
  
    case ASwitch(cond, clauses, l) =>
      NLT(cond.an, l) ::
      traversal(cond,env) :::
      clauses.map(x => x.rhs.map(y => NLT(cond.an, y.an))).flatten :::
      clauses.foldLeft(List[Constraint]())((r,x) => 
        x.rhs.foldLeft(List[Constraint]())((r1,x1) => traversal(x1,env) ::: r1) ::: r)
  }

  // Use the current known information to solve constraints, until reach a static state
  def solveConstraint(ccs:List[Constraint], constraints:List[Constraint]):(List[Constraint], List[Constraint]) = {
    var knowns = ccs
    // Constraints yet to be solved
    var nlts = constraints
    var lastSize = knowns.length
    do{
    lastSize = knowns.length
    for(x <- nlts){
      x match{
	case NLT(l1,l2) => {
	  // (x known && z <= x) => z known
	  if(knowns.contains(Known(l2))){
	    knowns = Known(l1) :: knowns
	    nlts = nlts.filter(x => x != NLT(l1,l2))
	  }
	  // (x known && x <= z) => z <= z
	  if(knowns.contains(Known(l1))){
	    nlts = NLT(l2,l2) :: nlts
	    nlts = nlts.filter(x => x != NLT(l1,l2))
	  }
	  // (x unknown && x <= z) => z unknown
	  if(knowns.contains(Unknown(l1))){
	    knowns = Unknown(l2) :: knowns
	    nlts = nlts.filter(x => x != NLT(l1,l2))
	  }

	}
	case _ => error(x + "is a known constriant")
      }
    }
    }while(lastSize < knowns.length)
    (knowns,nlts)
  }
  // Transform a arbitray constriant set into consistent constraint set form
  def CCS(cs:List[Constraint]):List[Constraint] = {
    var ccs:List[Constraint] = cs.filter(x => x.isInstanceOf[Known] ||
				              x.isInstanceOf[Unknown])
    var rest = cs.filter(!ccs.contains(_))
    // Use static constraint (known or unknown to deduct more information)
    var newKnown = List[Constraint]()
    var lastCcs  = List[Constraint]()
    do{
    lastCcs = ccs
    val solveOne = solveConstraint(ccs,rest)
    ccs = solveOne._1
    rest = solveOne._2
    // For any label x, if it only occurs in z <= z, it becomes known(x) 
    rest = ccsHelper(rest)
    newKnown = rest.filter(x => x.isInstanceOf[Known] ||
			       x.isInstanceOf[Unknown])
    // Check contradiction
    newKnown.map(x => x match{
      case Known(l) => if(ccs.contains(Unknown(l))) sys.error("Contracdition with label:" + l)
      case Unknown(l) => if(ccs.contains(Known(l))) sys.error("Contracdition with label:" + l)
    })
    ccs = (newKnown ::: ccs).toSet.toList
    rest = rest.filter(!newKnown.contains(_))
    }while(lastCcs.size < ccs.size)
    // Make unsolved constraints to unknown
    ccs ::: mkUnknown(rest)
  }
  // For any label x, if it only occurs in z <= z, it becomes known(x) 
  def ccsHelper(ccs:List[Constraint]):List[Constraint] = {
    var equalcs = ccs.filter(x => x match{
		    case NLT(l1,l2) => l1 == l2
		    case _ => false
		  }) 
    // Only keep z<=z, when z is not appearing in any x <= z
    equalcs = equalcs.filter(c => c match{
		case NLT(l1,l2) => !(ccs.exists(x => x match{
		  case NLT(l3,l4) => (l4 == l1 && l3 != l1)
		  case _ => false  
		}))
               case _ => false
	      })
    // In the input ccs, replace z <= z with known(z)
    ccs.map(c => c match{
      case NLT(l1,l2) => 
	if(l1 == l2 && equalcs.contains(NLT(l1,l2))){
	  Known(l1)
	}
	else
	  c
      case _ => c
    })
   
  }
 // Unsolved NLT constraint to unknown
 def mkUnknown(cs:List[Constraint]):List[Constraint]= {
    cs.map(x => x match{
      case NLT(l1, l2) => Unknown(l1) :: Unknown(l2) :: List[Constraint]()
      case _ => sys.error("Fail to solve constraints")
    }).flatten.toSet.toList
  }

 object Specialization {
   import acumen.interpreters.Common._
   // Graph structure for topological sorting
   case class Graph(vs:List[Vertex])
   case class Edge(val from:Label, val dest:Label)
   case class Vertex(val label:Label, val edges:List[Edge]) {
     def addEdge(e:Edge) = Vertex(label, e::edges)
     def deleteEdge(e:Edge) = Vertex(label, edges.filter(x => x != e))
     def hasNoIncomingEdges:Boolean = 
       !edges.exists(x => x.dest == label)
   }


   // Topological sort a list of equations, based on their dependence realations
   // Assumption: All the equations are staticly known, that is, no variables in those equations
   // which don't have a assginment equation to calculate it
   def topoSort(es:List[AEquation[Label]]):List[Label] = {
     def mkEdges(vs:List[AVar[Label]], env:Map[Expr,Label], label:Label):List[Edge]={
       vs.map(x => env.get(x.expr) match{
	 case Some(l) => Edge(l,label)
	 case None => error("variable " + x + " is unknown")
       })
     }

     def equationsToGraph(es:List[AEquation[Label]]) = {
       // Variables to their assigning equation's label
       val env = es.map(x => x match{
	 case AEquation(lhs,rhs,l) => (lhs.expr,l)}).toMap
       // Each equation is a vertex
       val vertexes:Map[Label, Vertex] = es.map(x => x match{
	 case AEquation(lhs,rhs,l) => (l, Vertex(l, mkEdges(findAVars(rhs),env,l)))
       }).toMap
       // Add dependence (incoming) edges
       for(e <- es){
	 e match{
	   case AEquation(lhs,rhs,l) => 
	     findAVars(rhs).map(x => vertexes(l).addEdge(Edge(env(lhs.expr), l)))
	 }
       }
        vertexes.values.toList
     }
     // Topological sort algorithm
     var graph:List[Vertex] = equationsToGraph(es)
     // Empty list that will contain the sorted labels
     var L:List[Label] = List[Label]()
     // List of vertexes with no incoming edges
     var S:Set[Vertex] = graph.filter(x => x.hasNoIncomingEdges).toSet
     // Main loop
     while(S.size > 0){
       val n:Vertex = S.head
       S = S.drop(1)
       L = n.label :: L
       for(m <- graph
	   if m.edges.exists(x => x.from == n.label)){
	 val edge = m.edges.find(e => e.from == n.label).get
	 // Delete edge n -> m
	 graph = graph.updated(graph.indexOf(m), m.deleteEdge(edge))
	 if(m.deleteEdge(edge).hasNoIncomingEdges)
	   S = S + m
       }
     }   
      // graph has edges => at least one cycle
    if(graph.exists(x => x.edges.length != 0))
      error("Has cycle in graph")
    else
      L.reverse
  }

 // Test whether all expr in list are known litertures
 def exprsToValues(exprs:List[Expr]):Option[List[Value[_]]] = {
   val lits:List[Lit] = exprs.filter(e => e.isInstanceOf[Lit]).asInstanceOf[List[Lit]]
   val values = lits.map(x => x match{
     case Lit(gv) => VLit(gv)
   })
   if(values.size == exprs.length)
     Some(values)
   else
     None
 }
 // Symbolic differentiation
 import SD._

 // Specialization based on BTA result,
 // Assumption:  Equations are reordered based on dependence
 def specialize(aexpr:AExpr[Label], bta:Map[Label,Constraint], env:Map[Expr,Expr]):Expr = 
   aexpr match{
     case ALit(gv,l) => Lit(gv)
     case AVar(name,l) => bta(l) match{
       // The value of a known variable should be available right now
       case Known(_) => env(Var(name))
       case Unknown(_) => env.get(Var(name)) match{
	 case Some(e) => e
	 case None => Var(name)
       }
     }
     case ADot(aer,name,l) => Dot(aer.expr,name)
     case AExprVector(es,l) => ExprVector(es.map(specialize(_,bta,env)))
     case AOp(f,aes,l) => bta(l) match{
      case Known(_) => f.x match{
	// Perform vector lookup
	case "index" => {
	  val es = aes.map(specialize(_,bta,env))
	  // TODO: Try catch here
	  (es(0), es(1)) match{
	    case (ExprVector(ls), Lit(GInt(i))) => ls(i)
	    case _ => error("Fail to perform vector lookup in "+ es(0) + " of " + es(1))
	  }
	}
	case "dif" => aes.map(specialize(_,bta,env)) match{
	  // TODO: Fix dt function to not take a list of variables as parameter
	  case e :: Nil => SD.dt(e)
	  case e :: v :: Nil => v match{
	    case Var(n) =>  SD.dif(e)(n)
	    case _ => error(v + " is not a variable for partial differentiation")
	  }
	  case _ => error("Can't perform symbolic differentiation on " + aexpr)
	}
	case _ => {
	  val es = aes.map(specialize(_,bta,env))
	  // Perform eval when all elments are reduced to Lit
	  exprsToValues(es) match{
	    case Some(ls) => evalOp(f.x, ls) match{
	      case VLit(gv) => Lit(gv)
              //case VVector(l)
	    }
	    case None => Op(f,es)
	  }
	}
      }
      case Unknown(_) => f.x match{
	case "dif" => aes.map(specialize(_,bta,env)) match{
	  // TODO: Fix dt function to not take a list of variables as parameter
	  case e :: Nil => SD.dt(e)
	  case e :: v :: Nil => v match{
	    case Var(n) =>  SD.dif(e)(n)
	    case _ => error(v + " is not a variable for partial differentiation")
	  }
	  case _ => error("Can't perform symbolic differentiation on " + aexpr)
	}

	case _ => Op(f,aes.map(specialize(_,bta,env)))
      } 
    }
   }
   // Specilize equation, return reuslt and argumented Env
   def specialize(aequation:AnContinuousAction[Label], bta:Map[Label,Constraint], env:Map[Expr,Expr]):(Action, Map[Expr,Expr]) = 
     aequation match{
       case AEquation(lhs,rhs,l) => lhs match{
	 // For directed equations
	 case AVar(name,lv) => 
	   val srhs = specialize(rhs,bta,env) 
	   (Continuously(Equation(lhs.expr, srhs)), env + (lhs.expr -> srhs))
		    
	 case _ => (Continuously(Equation(specialize(lhs,bta,env), 
			                   specialize(rhs,bta,env))),
		    env)
       }
     }

  def specialize(aaction:AAction[Label], bta:Map[Label,Constraint], env:Map[Expr,Expr]):(List[Action], Map[Expr,Expr]) = 
    aaction match{
      case AContinuously(c,l) => 
	val result = specialize(c,bta,env)
	(List(result._1), result._2)
      case ADiscretely(c,l) => c match{
	case AAssign(lhs,rhs,l1) => (List(Discretely(Assign(lhs.expr, rhs.expr))), env)
	case ACreate(x,name,args,l) => (List(Discretely(Create(x,name,args))),env)
	case AElim(ae,l) => (List(Discretely(Elim(ae.expr))),env)
	case AMove(aobj,ap,l) => (List(Discretely(Move(aobj.expr,ap.expr))),env)
      } 
      case AEquationFamily(i,q,aes,l) => {
	val vector:ExprVector = env.get(q.expr) match{
	  case Some(e) => e match{
	    case ExprVector(es) => ExprVector(es)
	  }
	  case None => error(q + " is not a known value")
	}
      var newActions = List[Action]()
      for (j <- 0 to vector.l.length-1){
        val newEnv = env + (Var(i.name) -> Lit(GInt(j)))
	newActions = (aes.map(a => specialize(a,bta,newEnv)._1)) ::: newActions  
      }
      (newActions, env)
      }
     case AIfThenElse(cond,t,e,l) => bta(cond.an) match{
       case Known(_) => specialize(cond,bta,env) match{
	 case Lit(GBool(true)) => (t.map(specialize(_,bta,env)._1).flatten,env)
	 case Lit(GBool(false)) =>(e.map(specialize(_,bta,env)._1).flatten,env)
       }

      case Unknown(_) => (List(IfThenElse(specialize(cond,bta,env),
				    t.map(specialize(_,bta,env)._1).flatten,
				    e.map(specialize(_,bta,env)._1).flatten)),
			  env)
     }
      case ASwitch(cond, clauses,l) => bta(cond.an) match{
	case Unknown(_) => (List(Switch(specialize(cond,bta,env),
				 clauses.map(c => Clause(c.lhs, specialize(c.assertion,bta,env),
							 c.rhs.map(specialize(_,bta,env)._1).flatten)))),			  
			    env)
	case Known(_) => {
	  specialize(cond,bta,env) match{
	    case Lit(gv) =>{ 
	      if(clauses.exists(x => x.lhs == gv)){
		val clause = clauses.find(x => x.lhs == gv).get
		(clause.rhs.map(specialize(_,bta,env)._1).flatten, env)
	      }
	      else{
		error("No matching clause for switch expr" + 
		      cond.toString)
	      }}
	  }
	}
      }
    }

 // Find AVars in the expr
 def findAVars(aexpr:AExpr[Label]):List[AVar[Label]]= aexpr match{
   case ALit(n,l) => List.empty
   case AVar(n,l) => List(AVar(n,l))
   case AOp(f,es,l) => es.map(findAVars(_)).flatten.toSet.toList
   // Explicit vector is considered knwon, thus will not add depenence edge to graph(fix me)
   case AExprVector(_,_) => List[AVar[Label]]()
   case _ => Nil
}

 def findVars(expr:Expr):List[Var]= expr match{
   case Lit(n) => List.empty
   case Var(n) => List(Var(n))
   case Op(f,es) => es.map(findVars(_)).flatten.toSet.toList
   // Explicit vector is considered knwon, thus will not add depenence edge to graph(fix me)
   case _ => Nil
 }

}
}

