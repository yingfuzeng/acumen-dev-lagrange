package acumen
import Array._
import TestSuiteForDAE._
import util.Names.name
import Pretty._
import acumen.interpreters.Common._
import BindingTimeAnalysis.Specialization._
/* Gaussian elimination */
object GE{
  /* Coefficiant matrix */
  var M:Matrix = empty[Array[Expr]]
  var B:Array[Expr] = empty[Expr]
     
  val zero = Lit(GInt(0))

  type Matrix = Array[Array[Expr]]

  def init(es:List[Equation], q:List[Var])={
    
    println("Input equations")
    val ses = es.map(x => Equation(simplify(x.lhs),
				   simplify(x.rhs)))
    //ses.map(x => println(pprint(x.lhs) + " = " + pprint(x.rhs)))       
    val inputTuple = equationsToMatrix(ses,q)
    M = inputTuple._1
    B = inputTuple._2
    println("Initial matrix:")
    printMatrix
   
  }

  /* Transform a set of equations into a coefficiant matrix B and rhs array.
  *  Requriment(IMPORTANT): the lhs of any input equation should be in the form of
  *  Op("+", c1*x1 :: c2*x2 :: ... cn*xn :: Nil)
  */
 def equationsToMatrix(es:List[Equation], q:List[Var]):(Matrix, Array[Expr]) = {
    // Size of Matrix N * N
    val N = es.length
    if(N != q.length)
      // TODO: Redesign error messages 
      sys.error("Not enough equations to solve unknown variables")

    // Initialize zero coefficiant matrix and rhs array
    var B:Array[Expr] = fill[Expr](N)(zero)
    var M:Matrix = fill[Array[Expr]](N)(fill[Expr](N)(zero))
    // Update coefficiant
    for(i <- 0 to N-1){
      B(i) = es(i).rhs
      for(j <- 0 to N-1){
	val coefs =  getCoef(es(i).lhs,q(j))
	if(coefs.length > 0)
	  M(i)(j) = mkPlus(coefs)
      }
   }
    // Return
    (M,B)
  }

  /* Example: 3*x => 3 */
  // TODO: Handle 2*x*5 => 2*5 etc.
  def getCoef(e:Expr,v:Var):List[Expr] = e match{
    case Op(Name("*",0), c::v1::Nil) => {if(v1 == v) List(c); else if(c == v) List(v1); else Nil }
    case v1:Var => {if(v1.name == v.name) List(Lit(GInt(1))); else Nil}
    case Op(Name("+",0), es) => es.foldLeft(List[Expr]())((r,x) => r ::: getCoef(x,v))
    case _ =>  error("Wrong format of " + e.toString + " to get coef")
  }

  /* A helper function to allow us to write Op("+", ...) instead of Op(Nmae("+",0),...) */
  implicit def stringToName(op:String) = Name(op,0)

  /* Length measure for pivoting:
   * In the case of numerical stability, the largest (in absolute value)
   * is chosen, whereas for coeffi- cient growth cases, it is the smallest
   * (according to some given size metric) that is chosen.
   * Here we choose the number of symbolic terms as a length measure*/
  def length(e:Expr):Int= e match{
    case Op(_,es) => es.length
    case _ => 1
  }

  /* A superfical way for checking zero entry
   * Todo: What about l*sin(t), where it might becomes zero at certain time?*/
  def isZero(e:Expr) = e match{
    case Lit(GInt(0)) => true
    case _ => false
  }

  /* Swap row i and j*/
  def swap(m:Matrix, i:Int, j:Int) = {
    val temp = m(i)
    m(i) = m(j)
    m(j) = temp 
  }

  def mkOp(o: String, xs: Expr*) ={
    val sxs = xs.toList.map(x => simplify(x))
    simplify(Op(Name(o, 0), xs.toList))
  }
 
  def printMatrix = {
    val N = M.length
    println("********************************")
    for(i <- 0 to N-1){
      for(j <- 0 to N-1)
        print(pprint(M(i)(j)) + " ")
      print(pprint(B(i)))
      println(" ")
    }
  }

  def normalize(mpp:Expr,mp:Array[Expr]):Array[Expr] = {
    var normalized:Array[Expr] = new Array[Expr](mp.length)
    for(i <- 0 to mp.length - 1){
      normalized(i) = mkOp("/",mp(i),mpp)
    }
    normalized
  } 

 // Simplify a constant expression, example 2 + 2 => 4
 def evalConstant(exp:Expr):Lit = exp match{
   case Lit(n) => Lit(n) 
   case Op(f,es) => exprsToValues(es.map(x => evalConstant(x))) match{
     case Some(ls) => evalOp(f.x, ls) match{
       case VLit(gv) => Lit(gv)
     }
     case None => error("Problem to statically eval " + exp)
   }
   case _ => error("Can't statically eval " + exp)

 }

  /* Example 2 * (3*x) => (2*3, x)*/
  def evalTrueVarTerm(exp:Expr, trueVar:Var):(Expr,Var) = {
     exp match{
       case Var(trueVar.name) => (Lit(GInt(1)), trueVar)
       case Op(Name("*",0), e1 :: e2 :: Nil) => (e1, e2) match{
	 case (Var(trueVar.name), _) => (e2, trueVar)
	 case (_,Var(trueVar.name)) => (e1, trueVar)
	 case _ => 
	   if(findVars(e1).contains(trueVar)){
	     val runE1 = evalTrueVarTerm(e1,trueVar)
	     (mkOp("*",runE1._1,e2),
	      runE1._2)
	   
	   }
	   else{
	     val runE2 = evalTrueVarTerm(e2,trueVar)
	     (mkOp("*",runE2._1,e1),
	      runE2._2)
	   
	   }
       }
     }

  }
  /* Divide an expr into a list of variable terms and a constant */
  def normalizeExpr(e:Expr, q:List[Var]):(Option[List[Expr]], Expr) = {
    var finalVarTerms:Option[List[Expr]] = None
    var finalConst  = Lit(GInt(0))
    val terms = breakExpr(e,q)
    // Find all the constants terms
    var constants = terms.filter(x => findVars(x).length == 0)
    var varTerms = terms.filter(x => findVars(x).length > 0)
    varTerms = varTerms.map(x => mkOp("*",evalVariableTerm(x)._1, evalVariableTerm(x)._2))
    // Terms contain true variables
    var trueVarTerms = varTerms.filter(x => findVars(x).exists(y => q.contains(y)))
    val constVarTerms = varTerms.filterNot(x => findVars(x).exists(y => q.contains(y)))
    println("Constant var terms:")
    constVarTerms.map(x => println(pprint(x)))
    val trueVars = trueVarTerms.map(x => 
      evalTrueVarTerm(x, 
		      findVars(x).find(y => q.contains(y)).get))
    println("Vars to be solved") 
    trueVars.map(x => println(pprint(x._1) + "*" + x._2.name.toString))
    constants = constants.map(evalConstant(_))
    if(constants.length > 0)
      finalConst = evalConstant(constants.drop(1).foldLeft(constants(0))((r,x) => 
							  Op(Name("+",0),r::x::Nil)))
    if(varTerms.length > 0)
      finalVarTerms = Some(trueVars.map(x => mkOp("*",x._1,x._2)))
    
    if(constVarTerms.length > 0){
      (finalVarTerms, mkOp("+",combineConstVarTerms(constVarTerms),finalConst))
    }
    else
      (finalVarTerms, finalConst)
  }
  /* Nomalize an arbitray equation  */
  def normalizeEquation(e:Equation, q:List[Var]):Equation = {
    (normalizeExpr(e.lhs,q), normalizeExpr(e.rhs,q)) match {
      case ((Some(vs),cl),(None, cr)) => 
	Equation(mkPlus(vs),
	         mkOp("-",cr,cl))
    }
  }

  /* Example: 2*x*y - 2*x*y => 0 */
  def combineConstVarTerms(varTerms:List[Expr]):Expr = {
    /* Break a const var term into coef * List(prim function) form */
    def breakConstVarTerm(varTerm:Expr):(Double, List[Expr]) = varTerm match{
      case Var(n) => (1.toDouble, List(Var(n)))
      case Op(Name("*",0), Lit(GInt(n)) :: t :: Nil) => (n.toDouble * breakConstVarTerm(t)._1,breakConstVarTerm(t)._2)
      case Op(Name("*",0), Lit(GDouble(n)) :: t :: Nil) => (n.toDouble * breakConstVarTerm(t)._1,breakConstVarTerm(t)._2)
      case Op(Name("*",0), t :: Lit(GInt(n)) :: Nil) => (n.toDouble * breakConstVarTerm(t)._1,breakConstVarTerm(t)._2)
      case Op(Name("*",0), t :: Lit(GDouble(n)) :: Nil) => (n.toDouble * breakConstVarTerm(t)._1,breakConstVarTerm(t)._2)
      // Prim function
      case Op(f,t::Nil) => (1.toDouble, List(Op(f,List(t))))
      case Op(Name("^",0), x::n::Nil) => (1.toDouble, List(Op(Name("^",0),List(x,n))))
      case Op(Name("*",0), t1 :: t2 :: Nil) => 
	val bt1 = breakConstVarTerm(t1)
	val bt2 = breakConstVarTerm(t2)
	(bt1._1 * bt2._1, bt1._2 :::  bt2._2)
      case _ => error("Constant variable term " + pprint(varTerm) + " is not in basic form")
    }

    var normalTerms = varTerms.map(breakConstVarTerm(_))
    var tempTerms = List[Expr]()
    var usedTerms = Set[List[Expr]]()
    for(x <- normalTerms){
      if(!usedTerms.contains(x._2)){
	// Find all the terms with the same prim function composition as x's
	val sameComps = normalTerms.filter(y =>x._2.foldLeft(x._2.size == y._2.size)(_ && y._2.contains(_)) )
	usedTerms = usedTerms ++ sameComps.map(x => x._2) 
	println("Size is " + sameComps.length + " with " + x._1.toString + "***" + x._2.map(y => println(pprint(y))))
	// Combine them together
	tempTerms = mkOp("*", Lit(GDouble(sameComps.drop(1).foldLeft(sameComps(0)._1)((r,x) => 
	                                                    r.toDouble + x._1))), mkTimes(x._2)) :: tempTerms
      }
    }
    mkPlus(tempTerms)
  }
  
  /* Example: (x,2,3,y)  => ((x + 2) + 3) + y*/
  def mkPlus(es:List[Expr]):Expr = {
    es.drop(1).foldLeft(es(0))((r,x) => 
                                    mkOp("+",r,x))
  }
  def mkTimes(es:List[Expr]):Expr = {
    es.drop(1).foldLeft(es(0))((r,x) => 
                                    mkOp("*",r,x))
  }

  /* Main GE algorithm */
  def run(es:List[Equation], q:List[Var]):List[Equation]= {
    println("Running GE algorithm")
    // Testing
    //val q = TestSuiteForDAE.test6q
    //val test = TestSuiteForDAE.test6
    val ses = es.map(x => normalizeEquation(x,q))
    println("Simplfied equations:")
    ses.map(x => println(pprint(x.lhs) + " = " + pprint(x.rhs)))
    init(ses,q)
    val N = M.length
    for(p <- 0 to N-1){
      // find pivot row and swap
      var max = p
      for(i <- p+1 to N-1){
	if(length(M(i)(p)) > length(M(max)(p)) || isZero(M(max)(p)))
	  max = i
      }
    swap(M,p,max)
    // Check singularity here
    if(isZero(M(p)(p)))
      // TODO: Refine error message
      sys.error("Matrix is singular can't be solved")
    val temp = B(p); B(p) = B(max); B(max) = temp
    // Normalization
    B(p) = mkOp("/",B(p), M(p)(p))   
    M(p) = normalize(M(p)(p),M(p))
    printMatrix
    // Pivot within M
    for(i <- p+1 to N-1){
      val alpha = mkOp("/",M(i)(p),M(p)(p))
      B(i) = mkOp("-", B(i), mkOp("*", alpha, B(p)))
      for(j <- p to N-1)
	if(j == p)
	  M(i)(j) = zero 
	else
	  M(i)(j) = mkOp("-", M(i)(j), mkOp("*", alpha, M(p)(j)))
    }
    printMatrix
   
    }
    printMatrix
    // Back substitution
    var x = new Array[Expr](N)
    for(i <- N-1 to 0 by -1){
      var sum:Expr = Lit(GInt(0))
      for(j <- i+1 to N-1)
	sum = mkOp("+", mkOp("*", M(i)(j), x(j)), sum)
      
      x(i) = mkOp("-", B(i),sum)
    }

   
    // Output equations
    var output = List[Equation]()
    for(i <- 0 to N-1){
      output = Equation(q(i),x(i)) :: output
    }
    output = output.reverse
    println("Result of GE ******************")
    output.map(x => println(pprint(x.lhs) + " = " + pprint(x.rhs)))
    output
  }
  // Test whether exp has any variables from vars in it
  def hasVar(exp:Expr, vars:List[Var]):Boolean = {
    findVars(exp).exists(x => vars.contains(x))
  } 

  // Break an expression into a list of basic terms, which are either constant or constant * variable
  def breakExpr(exp:Expr,vars:List[Var]):List[Expr] = exp match{
    case Lit(_) => List(exp)
    case Var(n) => List(exp)
    case Op(f,es) =>
      // Simply the expr when all the es are constant
      exprsToValues(es) match{
	case Some(ls) => evalOp(f.x, ls) match{
          case VLit(gv) => List(Lit(gv))
        }
 
        case None => f.x match{
	  case "+" => es.foldLeft(List[Expr]())((r,x) => r ::: breakExpr(x,vars))
	  case "-" => breakExpr(es(0),vars) :::
		      es.drop(1).foldLeft(List[Expr]())((r,x) => r ::: 
							breakExpr(mkOp("*", Lit(GInt(-1)),x),vars))
	  case "*" => es match{
	    case e1 :: e2 :: Nil => {
	      // Base case, example 2 * x 
	      if(vars.contains(e1) || vars.contains(e2))
		List(exp)
	      else{
		if(findVars(e1).length > 0 || findVars(e2).length > 0){
		  val e1l = breakExpr(e1,vars)
		  val e2l = breakExpr(e2,vars)
		  if(e1l.length < e2l.length)
		    e1l.map(x => e2l.map(y => mkOp("*", x,y))).flatten
		  
		  else
		    e2l.map(x => e1l.map(y => mkOp("*", x,y))).flatten
		}		  
		else
		  List(exp)
	      }

	    }
            // 1 * 2 * x case 
            case _ => error("FIX ME LATTER")
	  }
          
          //case "/" =>
	  // Other primitive functions
          case _ => List(exp)
	}
    }
  }

// Example: 2 * 3 * x => 6 * x
def evalVariableTerm(exp:Expr):(Expr,Expr) = exp match{
  case Var(n) => (Lit(GInt(1)),Var(n))
  case Op(f,es) => f.x match{
    case "*" => es match{
      case el::er::Nil => 
	if(findVars(el).length > 0 && findVars(er).length == 0){
	  (evalConstant(mkOp("*", er, evalVariableTerm(el)._1)),
	   evalVariableTerm(el)._2)
	  
	}
	else if(findVars(er).length > 0 && findVars(el).length == 0){
	  (evalConstant(mkOp("*", el, evalVariableTerm(er)._1)),
	   evalVariableTerm(er)._2)

	}
       else{
	  val lhs = evalVariableTerm(el)
	  val rhs = evalVariableTerm(er)
	  (evalConstant(mkOp("*",lhs._1,rhs._1 )),
	   mkOp("*",lhs._2,rhs._2))
	}
    }
    case _ =>  (Lit(GInt(1)),exp)
  }
 case _ =>  error(exp.toString + " is not a basic term")
}

def simplify(exp:Expr):Expr = {

  exp match{
    case Op(Name(f,0), x::y::Nil) =>   f match{
    case "+" => (x,y) match{
      case (Lit(GInt(n1)),Lit(GInt(n2))) => Lit(GInt(n1+n2))
      case (Lit(GInt(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1+n2))
      case (Lit(GDouble(n1)),Lit(GInt(n2))) => Lit(GDouble(n1+n2))
      case (Lit(GDouble(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1+n2))
      case (Lit(GInt(0)), n2)=> n2
      case (n1, Lit(GDouble(0))) => n1
      case (Lit(GDouble(0)), n2)=> n2
      case (n1, Lit(GInt(0))) => n1

      case _ => Op(Name(f,0), x::y::Nil)
    }
     case "-" => (x,y) match{
      case (Lit(GInt(n1)),Lit(GInt(n2))) => Lit(GInt(n1-n2))
      case (Lit(GInt(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1-n2))
      case (Lit(GDouble(n1)),Lit(GInt(n2))) => Lit(GDouble(n1-n2))
      case (Lit(GDouble(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1-n2))
      case (Lit(GInt(0)), n2)=> Op(Name("*",0), Lit(GInt(-1))::n2::Nil)
      case (n1, Lit(GInt(0))) => n1
      case _ => Op(Name(f,0), x::y::Nil) 
    }
    case "*" =>
      (x,y) match{
      case (Lit(GInt(n1)),Lit(GInt(n2))) => Lit(GInt(n1*n2))
      case (Lit(GInt(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1*n2))
      case (Lit(GDouble(n1)),Lit(GInt(n2))) => Lit(GDouble(n1*n2))
      case (Lit(GDouble(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1*n2))
      case (Lit(GInt(0)), n2)=> Lit(GInt(0))
      case (Lit(GDouble(0)), n2)=> Lit(GInt(0))
      case (n1, Lit(GInt(0))) => Lit(GInt(0))
      case (n1, Lit(GDouble(0))) => Lit(GInt(0))
      case (Lit(GInt(n)), Op(Name("*",0), Lit(GInt(n1))::y::Nil)) => mkOp("*", Lit(GInt(n*n1)),y)
      case (Lit(GDouble(n)), Op(Name("*",0), Lit(GDouble(n1))::y::Nil)) => mkOp("*", Lit(GDouble(n*n1)),y)
      case _ => Op(Name(f,0), x::y::Nil)
    }
    case "/" =>  (x,y) match{
      case (Lit(GInt(n1)),Lit(GInt(n2))) => Lit(GDouble(n1.toDouble/n2.toDouble))
      case (Lit(GInt(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1.toDouble/n2.toDouble))
      case (Lit(GDouble(n1)),Lit(GInt(n2))) => Lit(GDouble(n1.toDouble/n2.toDouble))
      case (Lit(GDouble(n1)),Lit(GDouble(n2))) => Lit(GDouble(n1.toDouble/n2.toDouble))
      case (Lit(GInt(0)), n2)=> Lit(GInt(0))
      case (n1, Lit(GInt(0))) => error("Divided by zero")
      case _ =>
	if(x == y)
	  Lit(GInt(1))
	else
	  Op(Name(f,0), x::y::Nil)
    }
    case _ => Op(Name(f,0),x::y::Nil)

  }
  
    case Op(Name("+",0), args) => Op(name("+"), args map simplify)
    case Op(Name("-",0), args) => Op(name("-"), args map simplify)
    case Op(Name("*",0), args) => Op(name("*"), args map simplify)
    case Op(Name("/",0), args) => Op(name("/"), args map simplify)
    case _ => exp
   }

}
}

//  LocalWords:  coefficiant
