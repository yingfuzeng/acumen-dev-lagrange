(*    
	Model: Spring Dumper system (based on CMU 'control tutorials for matlab' page)
             http://www.engin.umich.edu/group/ctm/PID/PID.html
	Author: Angela Zhu
	Date: 2007/07/01
*)
simulation 
	starting time: 0.0 
	ending time: 2.0
	step size: 0.001

external ridl "spring_damper_control.ridl"
	reads  length_of_spring,  force_of_damper,
	            fPID_0, fPID_p, fPID_pd, fPID_pi, fPID_pid;
	writes control_force;       (* control force to be added on suspension spring *) 
	
	observes event  clock when t = 0;
	observes event  clock rate t = 0.01; (* whenever t changes by .01 *)

external matlab
	reads  length_of_spring,
	            control_force, 
	            force_of_spring;

boundary 
	x with x(0)=0, x'(0)= 0;      (* spring length *)

	fPID_pi with fPID_pi(0)= (Kp2-1)*f ;
	fPID_pid with fPID_pid(0)=(Kp3-1)*f ;

system (* Plant equations *)
	m=1;
	b=10;
	k=20;
	f=1;
	Kp = 300;
	Kd = 10;
	Ki = 70;
	Kp2 = 30;
	Kp3 = 350;
	Ki3 = 300;
	Kd3 = 50;

	length_change = 0 until t=0.1 -> 0  until t=2 -> 0;
	w = length_change;
	u = control_force;

	length_of_spring = x;
	force_of_spring = k*x;
	force_of_damper = b*x';

	fPID_0 = 0;
	fPID_p = (Kp-1)*f - Kp*x;
	fPID_pd =  (Kp-1)*f -Kd*x' - Kp*x;
	
	x'' = 1/m * (-b*x'-k*x+u);

	fPID_pi' = Ki*f - Kp2*x' - Ki*x;
	fPID_pid' = Ki3*f - Kd3*x'' - Kp3*x' - Ki3*x;

